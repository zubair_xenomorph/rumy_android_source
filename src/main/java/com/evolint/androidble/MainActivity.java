package com.evolint.androidble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    ListView l;
    BluetoothAdapter mBluetoothAdapter;
    private ScanSettings settings;
    private List<ScanFilter> filters;
    private BluetoothLeScanner mLeScanner;
    private btAdapter btAdapter;
    private Handler mHandler;
    private ArrayList<BluetoothDevice> btDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        l=(ListView)findViewById(R.id.list);
        btDevices = new ArrayList<BluetoothDevice>();
        mHandler = new Handler();
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Sorry! BLE is not supported in your phone", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Sorry! Bluetooth is not supported ", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);

            }
        }else {
            if (Build.VERSION.SDK_INT >= 21) {
                mLeScanner = mBluetoothAdapter.getBluetoothLeScanner();//mLEScanner will be needed to start and stop bluetooth scanning
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)//The scan mode can be one of SCAN_MODE_LOW_POWER, SCAN_MODE_BALANCED or SCAN_MODE_LOW_LATENCY.https://developer.android.com/reference/android/bluetooth/le/ScanSettings.html#SCAN_MODE_LOW_LATENCY
                        .build();
                filters = new ArrayList<ScanFilter>();//Criteria for filtering result from Bluetooth LE scans. A ScanFilter allows clients to restrict scan results to only those that are of interest to them.
                // https://developer.android.com/reference/android/bluetooth/le/ScanFilter.html
                //will be required as an argument to call startScan()
                Log.d("zubair sdk>21", "in sdk>21);");
            }
        }

        btAdapter = new btAdapter(this);
        l.setAdapter(btAdapter);
        l.setOnItemClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInf = getMenuInflater();
        mInf.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.scan)
        {
            scanBLE(true);
        }
        if(id==R.id.discon)
        {
            //disconnect();
        }
        if(id == R.id.exit)
        {
            finish();
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }

    private void scanBLE(final boolean enable) {
        if(enable){
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(Build.VERSION.SDK_INT<21){
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    }else{
                        mLeScanner.stopScan(mScanCallback);
                    }

                }
            },1000);
            if(Build.VERSION.SDK_INT<21){
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }else{
                mLeScanner.startScan(filters,settings,mScanCallback);
            }


        }else{
            if(Build.VERSION.SDK_INT<21){
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }else{
                mLeScanner.stopScan(mScanCallback);
            }

        }
    }
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

        }
    };

    private ScanCallback mScanCallback = new ScanCallback() {

        @Override
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ////******** This function will be Callback when a BLE advertisement has been found*****/////////
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d("zubair callbackType", String.valueOf(callbackType));
            Log.d("zubair result", result.toString());
            BluetoothDevice btDevice = result.getDevice();
            if(!btDevices.contains(btDevice)) {
                btDevices.add(btDevice);
                btAdapter.add(btDevice);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };

    @Override
        public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
            final Intent intent = new Intent(this, com.evolint.androidble.Control.class);
            intent.putExtra(com.evolint.androidble.Control.EXTRAS_DEVICE_NAME, btDevices.get(position).getName());
            intent.putExtra(com.evolint.androidble.Control.EXTRAS_DEVICE_ADDRESS, btDevices.get(position).getAddress());
            startActivity(intent);
        }



    class btAdapter extends ArrayAdapter<String> {
        private ArrayList<BluetoothDevice> mLeDevices;
        Context context;
        btAdapter(Context c){
            super(c,R.layout.single_row,R.id.bDev);
            mLeDevices=new ArrayList<BluetoothDevice>();
            context= c;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mView = infl.inflate(R.layout.single_row, parent,false );
            ImageView icon = (ImageView)mView.findViewById(R.id.bicon);
            TextView btDev =(TextView)mView.findViewById(R.id.bDev);
            TextView btAdd =(TextView)mView.findViewById(R.id.bAdd);
            icon.setImageResource(R.drawable.ble);
            btDev.setText(mLeDevices.get(position).getName());
            btAdd.setText(mLeDevices.get(position).getAddress());
            Log.d("zubair","creating view");

            return mView;
        }


        public void add(BluetoothDevice device) {
            Log.d("zubair", "adding device->");

            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
                Log.d("zubair", "adding device<-");
                super.add("");

            }
        }
    }
}

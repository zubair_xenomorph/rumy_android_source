package com.evolint.androidble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;

/**
 * Created by Zubair on 6/20/2016.
 */
public class bleService extends Service {
    private BluetoothManager bleManager;
    private BluetoothAdapter bleAdapter;
    private String prev_bleAddress;
    private BluetoothGatt bleGatt;

    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
    //public static String HC_08_RX_TX = "0000fff4-0000-1000-8000-00805f9b34fb";
    public static String RUMY_AMBIENT_TEMPERATURE = "00002a6e-0000-1000-8000-00805f9b34fb";
    public static String RUMY_DESIRED_TEMPERATURE = "0000beef-0000-1000-8000-00805f9b34fb";
    public static String RUMY_HUMIDITY = "00002a6f-0000-1000-8000-00805f9b34fb";
    public static String RUMY_LIGHT = "0000dead-0000-1000-8000-00805f9b34fb";

    //public final static UUID UUID_HC_08_RX_TX = UUID.fromString(HC_08_RX_TX);
    public final static UUID UUID_RUMY_AMBIENT_TEMPERATURE = UUID.fromString(RUMY_AMBIENT_TEMPERATURE);
    public final static UUID UUID_RUMY_DESIRED_TEMPERATURE_ = UUID.fromString(RUMY_DESIRED_TEMPERATURE);
    public final static UUID UUID_RUMY_HUMIDITY = UUID.fromString(RUMY_HUMIDITY);
    public final static UUID UUID_RUMY_LIGHT = UUID.fromString(RUMY_LIGHT);


    private final BluetoothGattCallback btGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d("zubair", "in onConnectionStateChange");

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d("zubair", "connectedddddddddd");
                sendBroadcast(new Intent("androidBLE.connected"));
                bleGatt.discoverServices();

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d("zubair","notnconnectedddddddddd");
                sendBroadcast(new Intent("androidBLE.disconnected"));
            }
            super.onConnectionStateChange(gatt, status, newState);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                sendBroadcast(new Intent("androidBLE.serviceDiscovered"));
                Log.d("zubair","service discovered gatt success");
            }
            Log.d("zubair", "service discovered ");
            super.onServicesDiscovered(gatt, status);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Intent intent = new Intent("androidBLE.dataRead");
            String value = new String(characteristic.getValue());
            intent.putExtra("data",value);
            sendBroadcast(intent);
            Log.d("zubair", "characterstics read outside" +value);
            super.onCharacteristicChanged(gatt, characteristic);
           if(characteristic.getUuid().equals(UUID_RUMY_DESIRED_TEMPERATURE_))
           {
               intent = new Intent("androidBLE.dataRead.dtemp");
               value = new String(characteristic.getValue());
               intent.putExtra("data",value);
               sendBroadcast(intent);
               Log.d("zubair", "characterstics read dtemp" +value);
               super.onCharacteristicChanged(gatt, characteristic);
           }
            else if(characteristic.getUuid().equals(UUID_RUMY_LIGHT))
           {
               intent = new Intent("androidBLE.dataRead.light");
               value = new String(characteristic.getValue());
               intent.putExtra("data",value);
               sendBroadcast(intent);
               Log.d("zubair", "characterstics read light" +value);
               super.onCharacteristicChanged(gatt, characteristic);
           }
           else if(characteristic.getUuid().equals(UUID_RUMY_AMBIENT_TEMPERATURE))
           {
               intent = new Intent("androidBLE.dataRead.atemp");
               value = new String(characteristic.getValue());
               intent.putExtra("data",value);
               sendBroadcast(intent);
               Log.d("zubair", "characterstics read atemp" +value);
               super.onCharacteristicChanged(gatt, characteristic);
           }
           else if(characteristic.getUuid().equals(UUID_RUMY_HUMIDITY))
           {
               intent = new Intent("androidBLE.dataRead.humid");
               value = new String(characteristic.getValue());
               intent.putExtra("data",value);
               sendBroadcast(intent);
               Log.d("zubair", "characterstics read humid" +value);
               super.onCharacteristicChanged(gatt, characteristic);
           }



        }
    };
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("zubair", "bound service");;
        return mBinder;
    }
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        bleService getService() {
            return bleService.this;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public void close() {
        if (bleGatt == null) {
            return;
        }
        bleGatt.close();
        bleGatt = null;
    }

    public boolean initialize() {
        if (bleManager == null) {
            bleManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (bleManager == null) {
                Log.e("zubair", "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        bleAdapter = bleManager.getAdapter();
        if (bleAdapter == null) {
            Log.e("zubair", "Unable to obtain a BluetoothAdapter.");
            return false;
        }
        return true;
    }

    public boolean connect(final String address) {
        if (bleAdapter == null || address == null) {
            Log.w("zubair", "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (prev_bleAddress != null && address.equals(prev_bleAddress)
                && bleGatt != null) {
            Log.d("zubair", "Trying to use an existing mBluetoothGatt for connection.");
            if (bleGatt.connect()) {
                //mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                final BluetoothDevice device = bleAdapter.getRemoteDevice(address);
                bleGatt = device.connectGatt(this, false, btGattCallback);
                prev_bleAddress = address;
                return false;
            }
        }

        final BluetoothDevice device =bleAdapter.getRemoteDevice(address);
        if (device == null) {
            Log.w("zubair", "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        bleGatt = device.connectGatt(this, false, btGattCallback);
        Log.d("zubair", "Trying to create a new connection.");
        prev_bleAddress = address;
        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        if (bleAdapter == null || bleGatt == null) {
            Log.w("zubair", "BluetoothAdapter not initialized");
            return;
        }
        bleGatt.disconnect();
    }

    public List<BluetoothGattService> getGattServices() {
        if (bleGatt == null) return null;

        return bleGatt.getServices();
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled) {
        if (bleAdapter == null || bleGatt == null) {
            Log.w("zubair", "BluetoothAdapter not initialized");
            return;
        }
        bleGatt.setCharacteristicNotification(characteristic, enabled);

        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
        Log.d("zubair",descriptor.toString());
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        bleGatt.writeDescriptor(descriptor);

        /*
        // This is specific to Heart Rate Measurement.
        if (UUID_HC_08_RX_TX.equals(characteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                    UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
            Log.d("zubair",descriptor.toString());
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            bleGatt.writeDescriptor(descriptor);
        }
        */
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (bleAdapter == null || bleGatt == null) {
            Log.w("zubair", "BluetoothAdapter not initialized");
            return;
        }

        bleGatt.writeCharacteristic(characteristic);
    }





}
